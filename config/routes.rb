Spree::Core::Engine.routes.draw do
  get '/reviews',  action: :show, controller: 'pages', p: 'reviews', :as => :reviews

  namespace :admin do
    resource :yotpo_settings do
      collection do
        post :upload_past_orders
      end
    end
  end
end
