module Spree
  module Admin
    class YotpoSettingsController < Spree::Admin::BaseController
      PAST_ORDERS_DAYS_BACK = 180

      def edit
      end

      def upload_past_orders(days_in_past: PAST_ORDERS_DAYS_BACK)
        client = YotpoAPI::V1::Client.new
        client.spree(spree)
        app_key = Rails.configuration.x.yotpo.app_key
        secret_key = Rails.configuration.x.yotpo.secret_key

        Spree::Order.where("completed_at > ?", PAST_ORDERS_DAYS_BACK.days.ago).each do |order|
          begin
            client.create_order(app_key, secret_key, order)
            Rails.logger.info "Yotpo: Successfully uploaded order: #{order.number}"
          rescue StandardError => e
            Rails.logger.info "Yotpo: Failed uploading order: #{order.number}"
            Rails.logger.info e
          end
        end

        head :no_content
      end
    end
  end
end
