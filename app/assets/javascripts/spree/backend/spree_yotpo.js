Spree.routes.upload_past_orders = Spree.adminPathFor('yotpo_settings/upload_past_orders')

/* global show_flash */
$(function () {
  $('[data-hook=yotpo_settings_upload_past_orders] #upload_past_orders').click(function () {
    if (confirm(Spree.translations.are_you_sure)) {
      $.post(Spree.routes.upload_past_orders)
        .done(function () {
          show_flash('success', 'Orders uploaded.  Check logs for errors.')
        })
        .fail(function (message) {
          if (message.responseJSON['error']) {
            show_flash('error', message.responseJSON['error'])
          } else {
            show_flash('error', 'There was a problem while uploading orders. Check logs for errors.')
          }
        })
    }
  })
})
