Deface::Override.new(
  virtual_path: 'spree/products/_product',
  name: 'add_yotpo_star_rating_to_products_list_item',
  insert_bottom: "[data-hook='products_list_item'] div.product-stats",
  partial: 'spree/shared/yotpo_product_show'
)
