Deface::Override.new(
  virtual_path: 'spree/home/index',
  name: 'add_yotpo_reviews_carousel_to_home',
  insert_after: "[data-hook='home_bottom']",
  partial: 'spree/shared/yotpo_reviews_carousel'
)
