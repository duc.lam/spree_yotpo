Deface::Override.new(
  virtual_path: 'spree/shared/_product_overview_sidebar',
  name: 'add_yotpo_star_rating_to_product_overview_sidebar',
  insert_after: "[data-hook='product_overview_sidebar'] div.sku",
  partial: 'spree/shared/yotpo_star_rating'
)
