Deface::Override.new(
  virtual_path: 'spree/products/show',
  name: 'add_yotpo_star_rating_rich_snippets_review__to_product_show',
  insert_after: "[data-hook='product_show'] div.sku",
  partial: 'spree/shared/yotpo_product_show'
)
