Deface::Override.new(
  virtual_path: 'spree/products/show',
  name: 'add_yotpo_reviews_widget_to_product_show',
  insert_bottom: "[data-hook='product_show']",
  partial: 'spree/shared/yotpo_reviews_widget'
)
