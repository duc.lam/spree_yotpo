Deface::Override.new(
  virtual_path: 'spree/shared/_footer',
  name: 'add_reviews_link_to_footer',
  insert_bottom: "[data-hook='footer'] ul.footer-nav",
  text: "<li><%= link_to 'Reviews', reviews_path %></li>"
)
