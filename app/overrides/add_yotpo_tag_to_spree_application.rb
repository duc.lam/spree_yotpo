Deface::Override.new(
  virtual_path: 'spree/layouts/spree_application',
  name: 'add_yotpo_tag_to_spree_application_inside_head',
  insert_bottom: "[data-hook='inside_head']",
  partial: 'spree/shared/yotpo_tag'
)
