# Based on:
# https://www.nopio.com/blog/how-to-create-an-api-wrapper-of-an-external-service-in-rails/
module YotpoAPI
  module V1
    class Client
      include HttpStatusCodes
      include ApiExceptions

      # Reviews - Merchant
      API_ENDPOINT = 'https://api.yotpo.com'.freeze

      # Reviews - Storefront
      API_ENDPOINT_STOREFRONT = 'https://api-cdn.yotpo.com/v1/widget'.freeze 

      def spree(app)
        @_spree = app
      end

      def rich_snippets(api_key, product_id)
        response = request(
          client_merchant,
          http_method: :get,
          endpoint: "products/#{api_key}/#{product_id}/bottomline"
        )
      end

      def reviews(api_key, product_id)
        response = request(
          client_storefront,
          http_method: :get,
          endpoint: "#{api_key}/products/#{product_id}/reviews.json?per_page=150&sort=rating&direction=desc"
        )
      end

      def create_order(api_key, secret_key, order)
        utoken = get_token(client_merchant, api_key, secret_key)
        params = order_json_params(order, utoken)
        
        post_json(client_merchant, "apps/#{api_key}/purchases", params)
      end

      private

      def client_merchant
        @_client_merchant ||= Faraday.new(API_ENDPOINT) do |client_merchant|
          client_merchant.request :url_encoded
          client_merchant.adapter Faraday.default_adapter
        end
      end

      def client_storefront
        @_client_storefront ||= Faraday.new(API_ENDPOINT_STOREFRONT) do |client_storefront|
          client_storefront.request :url_encoded
          client_storefront.adapter Faraday.default_adapter
        end
      end

      # request as url_encoded
      def request(client, http_method:, endpoint:, params: {})
        response = client.public_send(http_method, endpoint, params)
        parsed_response = Oj.load(response.body)

        return parsed_response if response_successful?(response)

        raise error_class(response, "Code: #{response.status}, request: #{params} response: #{response.body}")
      end

      # post as json
      def post_json(client, endpoint, params)
        response = client.post do |req|
          req.url endpoint
          req.headers['Content-Type'] = 'application/json'
          req.body = params
        end
        parsed_response = Oj.load(response.body)

        return parsed_response if response_successful?(response)

        raise error_class(response, "Code: #{response.status}, request: #{params}, response: #{response.body}")
      end

      def get_token(client, api_key, secret_key)
        params = '{ "client_id": "' + api_key + '", ' + '"client_secret": "' + secret_key + '", "grant_type": "client_credentials" }'
        ret = post_json(client, '/oauth/token', params)

        ret['access_token']
      end

      def error_class(response, message)
        case response.status
        when HTTP_BAD_REQUEST_CODE
          BadRequestError.new(message)
        when HTTP_UNAUTHORIZED_CODE
          UnauthorizedError.new(message)
        when HTTP_FORBIDDEN_CODE
          ForbiddenError.new(message)
        when HTTP_NOT_FOUND_CODE
          NotFoundError.new(message)
        when HTTP_UNPROCESSABLE_ENTITY_CODE
          UnprocessableEntityError.new(message)
        else
          ApiError.new(message)
        end
      end

      def response_successful?(response)
        response.status == HTTP_OK_CODE
      end

      def order_json_params(order, utoken)
        default_options = Rails.application.default_url_options
        host = default_options[:host]

        products_arr = '{ '

        order.line_items.each do |item|
          product_id = item.variant.product.id
          product_url = @_spree.product_url(host: host, id: product_id)
          product_name = item.name
          if item.variant.images.length == 0
            product_image = item.variant.product.images.first.my_cf_image_url("small")
          else
            product_image = item.variant.images.first.my_cf_image_url("small")
          end
          product_description = (item.variant.product.short_description.present? ? item.variant.product.short_description : item.variant.product.description)
          product_price = item.variant.product.price
          
          # Description with whitespaces/return line feed breaks YotPo.
          # Description is not used in YotPo. Send blank description per Emanuel@YotPo. 
          products_arr = products_arr + '"' + product_id.to_s + '": { ' +
                                        '"url": "' + product_url + '", ' + 
                                        '"name": "' + product_name + '", ' +
                                        '"image": "' + product_image + '", ' +
                                        '"description": "' + '' + '", ' +
                                        '"price": "' + product_price.to_s + '" }'
          products_arr = products_arr + ', '
        end

        substitute = ', '
        products_arr = products_arr.gsub(/[#{substitute}]+$/, '')
        products_arr = products_arr + ' }'
        
        json_params = '{ ' +
                        '"platform": "general",' +
                        '"utoken": "' + utoken + '",' +
                        '"email": "' + order.email + '",' +
                        '"customer_name": "' + order.name + '",' +
                        '"order_id": "' + order.number + '",' +
                        '"order_date": "' + order.completed_at.to_s + '",' +
                        '"currency_iso": "USD",' +
                        '"products": ' + products_arr +
                        ' }'

        json_params
      end
    end
  end
end
