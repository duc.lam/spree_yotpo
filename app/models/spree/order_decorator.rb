Spree::Order.class_eval do

  def yotpo_conversion_tracking
    client = YotpoAPI::V1::Client.new
    client.spree(Spree::Core::Engine.routes.url_helpers)
    app_key = Rails.configuration.x.yotpo.app_key
    secret_key = Rails.configuration.x.yotpo.secret_key

    begin
      client.create_order(app_key, secret_key, self)
      Rails.logger.info "Yotpo: Successfully track conversion order: #{self.number}"
    rescue StandardError => e
      Rails.logger.info "Yotpo: Failed track conversion order: #{self.number}"
      Rails.logger.info e
    end
  end
end

Spree::Order.state_machine.after_transition :to => :complete,
                                            :do => :yotpo_conversion_tracking