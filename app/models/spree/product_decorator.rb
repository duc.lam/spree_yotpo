Spree::Product.class_eval do
  def yotpo_rich_snippets()
    client = YotpoAPI::V1::Client.new
    app_key = Rails.configuration.x.yotpo.app_key
    expire = (Rails.configuration.x.yotpo.rich_snippets_refresh_time.present? ? Rails.configuration.x.yotpo.rich_snippets_refresh_time : 24.hours)

    begin
      rich_snippets = Rails.cache.fetch("#{cache_key}/yotpo_rich_snippets", expires_in: expire) do
        client.rich_snippets(app_key, id)
      end

      return rich_snippets
    rescue StandardError => e
      Rails.logger.warn "Yotpo: Failed to get rich snippets for product: #{id}"
      Rails.logger.warn e

      return {}
    end
  end

  def yotpo_reviews()
    client = YotpoAPI::V1::Client.new
    app_key = Rails.configuration.x.yotpo.app_key
    expire = (Rails.configuration.x.yotpo.reviews_refresh_time.present? ? Rails.configuration.x.yotpo.reviews_refresh_time : 24.hours)

    begin
      reviews = Rails.cache.fetch("#{cache_key}/yotpo_reviews", expires_in: expire) do
        client.reviews(app_key, id)
      end
      
      return reviews
    rescue StandardError => e
      Rails.logger.warn "Yotpo: Failed to get reviews for product: #{id}"
      Rails.logger.warn e

      return {}
    end
  end
end
